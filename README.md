# Umberto-hybrid-16S


E. coli 16S conserved region with preferably exposed part (defined by Ribo-seq) 
will be targeted with Cas13b. Try not to cut other rRNA like 5S + other mRNA -
but we don't care that much about off-targets.

PbuCas13b PFS in bacteria is not well defined, in human they have not observed it.
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5793859/
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5432119/
https://www.sciencedirect.com/science/article/pii/S2211124719302803

E. coli essential genes with Cas13b revealed that spacers with secondary 
structure near the protospacer had reduced depletion in the screen.

https://zlab.bio/cas13


Based on the suplementary from Slaymaker paper I think spacer is 31bp and no 
PFS.

PbuCas13b used is https://www.addgene.org/115209/
Alternatively, guides can be ordered as synthetic RNA from providers such as 
IDT or Synthego, and we have seen increased performance with synthesized RNA. 